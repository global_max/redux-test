var gulp = require('gulp');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var run_sequence = require('run-sequence');
var webserver = require('gulp-webserver');

var isProdEnv = false;

var bundles = {
	app: {
		src: './src/main.js'
	}
};

gulp.task('style', function () {
	return gulp.src('./less/main.less')
		.pipe(less())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./assets/css'));
});

gulp.task('script', function() {
	var stream = browserify({entries: './src/main.js', debug: !isProdEnv})
		.transform('babelify', {
			presets: ['es2015', 'react']
		})
		.bundle()
		.on('error', function(err) {
			console.error(err.toString());
		})
		.pipe(source('app.js'))
		.pipe(buffer());

	if (isProdEnv) {
		stream
			.pipe(uglify());
	}

	return stream.pipe(gulp.dest('./assets/js'));
});

gulp.task('webserver', function() {
	gulp.src('./')
		.pipe(webserver({
			livereload: true,
			open: true
		}));
});

gulp.task('watch', function () {
	gulp.watch('./less/**/*.less', ['style']);
	gulp.watch('./src/**/*.js', ['script']);
});

gulp.task('dev', function(callback) {
	run_sequence('style', 'script', 'webserver', 'watch');
});

gulp.task('prod', function(callback) {
	isProdEnv = true;
	run_sequence('style', 'script');
});