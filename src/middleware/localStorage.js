export default function localStorageMiddleware(key) {
	return ({getState}) => next => action => {
		const result = next(action);
		const nextState = Object.assign({}, getState());
		const storageValue = JSON.stringify(nextState);

		window.localStorage.setItem(key, storageValue);

		return result
	}
}