import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import MainSection from '../components/MainSection'
import * as AppActions from '../actions'

class App extends Component {
	render() {
		const { comments } = this.props;

		return (
			<section className="l-main l-main--padded">
				<MainSection
					comments={comments}
					onAddComment={this.addComment.bind(this)}
					onDeleteComment={this.deleteComment.bind(this)}/>
			</section>
		)
	}

	addComment(text) {
		const { actions } = this.props;

		actions.addComment(text);
	}

	deleteComment(id) {
		const { actions } = this.props;

		actions.deleteComment(id);
	}
}

App.propTypes = {
	comments: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
	return {
		comments: state.comments
	}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(AppActions, dispatch)
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App)