
import { ADD_COMMENT, DELETE_COMMENT } from '../constants/ActionTypes'

const initialState = [
	{
		author: 'Администратор',
		company: 'Сравни.ру',
		date: '13 мая 2016 09:45',
		text: 'Виталий, доброе утро! Напишите номер полиса, пожалуйста. Как долго являетесь клиентом данной СК?',
		id: 1,
		isAdministrator: true
	}, {
		author: 'Виталий Акатьев',
		company: 'Сравни.ру',
		date: '13 мая 2016 12:52',
		text: 'Здравствуйте! Полис ЕЕЕ 0372592961. Сколько точно являюсь клиентом сказать не могу, т.к. периодически менял страховую. Но страховых случая в Ренессансе было два. Последний в апреле сего года. ',
		id: 2,
		isAuthor: true
	}, {
		author: 'Представитель банка',
		company: 'Сравни.ру',
		date: '13 мая 2016 19:28',
		text: 'Виталий, добрый вечер. Спасибо, что нашли время оставить отзыв. Нам очень приятно, что Вы оценили качество наших услуг. ',
		id: 3,
		isRepresentative: true
	}
];

const dateOptions = {
	year: 'numeric',
	month: 'long',
	day: 'numeric',
	hour: 'numeric',
	minute: 'numeric'
};

export default function comments(state = initialState, action) {
	switch (action.type) {
		case ADD_COMMENT:
			return [
				...state,
				{
					id: state.reduce((maxId, comment) => Math.max(comment.id, maxId), -1) + 1,
					text: action.text,
					date: (new Date()).toLocaleString('ru', dateOptions),
					author: window.user.author,
					avatar: window.user.avatar
				}
			];

		case DELETE_COMMENT:
			return state.filter(comment =>
				comment.id !== action.id
			);

		default:
			return state
	}
}