import React, { Component, PropTypes } from 'react'
import Comment from '../components/Comment'
import CommentForm from '../components/CommentForm'

class CommentBox extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {text: ''}
	}

	render() {

		const { comments } = this.props;
		const { onAddComment, onDeleteComment } = this.props;

		const commentNodes = comments.map(comment => {
			return (
				<Comment
					{...comment}
					key={comment.id}
					onDeleteComment={onDeleteComment.bind(this)}
					onReply={this.onReply.bind(this)} />
			);
		});

		return (
			<div className="comments__box">

				<h1 className="comments__heading">Комментарии ({comments.length})</h1>

				<ul className="comments__list">

					{commentNodes}

				</ul>

				<CommentForm onAddComment={onAddComment} text={this.state.text} />

			</div>
		)
	}

	onReply(text) {
		this.setState({ text: text + ', ' });

		const element = document.querySelector('.comments__form__message__textarea');

		element.focus();
	}

}

CommentBox.propTypes = {
	comments: PropTypes.array.isRequired
};

export default CommentBox