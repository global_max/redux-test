import React, { Component, PropTypes } from 'react'
import { getRandomColor } from '../utils/color'

class Comment extends Component {
	constructor(props, context) {
		super(props, context);
		this.avatarColor = getRandomColor();
	}

	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.id !== this.props.id;
	}

	handleReply() {
		event.preventDefault();

		const { author } = this.props;
		const { onReply } = this.props;
		onReply(author);
	}

	handleDelete() {
		event.preventDefault();

		const { id } = this.props;
		const { onDeleteComment } = this.props;
		onDeleteComment(id);
	}

	get userName() {
		const { author, company, isAdministrator, isAuthor, isRepresentative } = this.props;

		if (isAuthor) {
			return '<span style="color: #e2ac2f">Автор</span>,'+ author +', ';
		} else if (isAdministrator) {
			return '<span style="color: #57c15a">Администратор</span>, ' + company +', ';
		} else if (isRepresentative) {
			return 'Представитель банка, ';
		} else {
			return author +', ';
		}

	}

	get avatarStub() {
		return (
			<div className="comments__item__author__avatar__stub" style={{backgroundColor: this.avatarColor}}>
				<svg viewBox="0 0 76.3 80">
					<path d="M76.3,80h-4V77c0-4.4-1.2-7.9-3.8-10.7c-2.2-2.4-5.3-4.4-10-6.3C57,59.4,47,55.9,38.1,55.9
					c-9.4,0-17.9,3.2-20.3,4.2C8.5,63.5,4,69,4,77V80H0V77c0-9.6,5.7-16.8,16.4-20.7c2.5-1,11.5-4.5,21.7-4.5c9.7,0,20.3,3.8,22,4.5
					c5.2,2.1,8.8,4.4,11.4,7.3c3.2,3.6,4.8,7.9,4.8,13.4V80z" fill="#FFFFFF"></path>
					<path d="M37.5,47.6c-4.8,0-9.4-2.3-12.9-6.6c-3.5-4.3-5.9-10.3-6.9-17.5c-0.8-6.2,0.8-12,4.8-16.5
					c3.8-4.3,9.4-6.9,15-6.9c5.6,0,11.2,2.6,15,6.9c3.9,4.5,5.6,10.3,4.7,16.5C55.5,37,46.8,47.6,37.5,47.6z M37.5,4
					c-4.5,0-9,2.1-12,5.6c-3.1,3.6-4.5,8.3-3.8,13.3c0.9,6.4,3,11.8,6,15.5c2.8,3.3,6.2,5.2,9.8,5.2c7,0,14.3-9.5,15.8-20.7
					c0.7-5-0.7-9.7-3.8-13.3C46.5,6.1,42,4,37.5,4z" fill="#FFFFFF"></path>
				</svg>
			</div>
		)
	}

	render() {
		const { avatar, author, date, text, id } = this.props;

		return (
			<li className="comments__item">
				<span className="comments__item__delete" onClick={this.handleDelete.bind(this)}></span>
				<div className="columns">

					<aside className="column-1">

						<ul className="comments__item__author">
							<li className="comments__item__author__avatar">
								{
									avatar
										? <img className="comments__item__author__avatar__picture" src={avatar} alt={author} />
										: this.avatarStub
								}
							</li>
						</ul>

					</aside>

					<section className="column-11">

						<header className="comments__item__header">
							<ul className="comments__item__header__meta">
								<li className="comments__item__header__meta__name" dangerouslySetInnerHTML={{__html: this.userName}}></li>
								<li className="comments__item__header__meta__date"> {date}</li>
							</ul>
						</header>

						<div className="comments__item__text">
							{text}
							<span
								className="comments__item__text__reply"
								onClick={this.handleReply.bind(this)}>Ответить</span>
						</div>

					</section>

				</div>
			</li>
		)
	}
}

Comment.propTypes = {
	author: PropTypes.string.isRequired
};

export default Comment