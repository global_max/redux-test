import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import Store from './store/Store'

const store = Store('test');

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('app')
);