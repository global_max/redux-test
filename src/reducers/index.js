import { combineReducers } from 'redux'
import comments from './comments'

const appReducer = combineReducers({
	comments
});

export default appReducer