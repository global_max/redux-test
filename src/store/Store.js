import {compose, createStore, applyMiddleware} from 'redux'
import storageMiddleware from '../middleware/localStorage'
import appReducer from '../reducers'

export default function configureStore(storageKey, initialState) {
	const localStorageState = window.localStorage.getItem(storageKey) && JSON.parse(window.localStorage.getItem(storageKey)) || {}
	const defaultState = Object.assign({}, localStorageState, initialState);

	let createStoreWithMiddleware = applyMiddleware(storageMiddleware(storageKey))(createStore);

	const store = createStoreWithMiddleware(appReducer, defaultState);

	return store
}


//export default function Store(initialState) {
//	const store = createStore(appReducer, initialState);
//
//	return store
//}