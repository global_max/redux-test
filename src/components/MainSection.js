import React, { Component, PropTypes } from 'react'
import CommentBox from '../components/CommentBox'

class MainSection extends Component {
	constructor(props, context) {
		super(props, context);
	}

	render() {
		const { comments } = this.props;
		const { onAddComment, onDeleteComment } = this.props;

		return (
			<div className="reviews__box">

				<aside className="column-2">

					<ul className="reviews__author">
						<li className="reviews__author__avatar">
							<img className="reviews__author__avatar__picture" src="/assets/img/avatar.jpg" alt="avatar.jpg" />
						</li>
						<li className="reviews__author__name">Maxim Denisov</li>
						<li className="reviews__author__location">Moscow</li>
					</ul>

				</aside>
				<section className="column-10">
					<header className="reviews__header">
						<div className="reviews__header__date">12 часов назад (09:21)</div>
						<h1 className="reviews__header__title">Лучшая страховая компания!</h1>
					</header>

					<div className="reviews__text">
						Не знаю как у кого. Но я второй раз обращаюсь в Ренессанс и оба раза удовлетворен. Оба раза по ОСАГО. В первый раз это была страховая виновника, второй раз моя. В обоих случаях я остался доволен работой Компании. Из минусов только то, что офис оценки ущерба в Москве только один на Дербеневской набарежной. Но зато приехал, осмотрели, оценили, подписали все нужные бумажки, уехал. В оговоренный срок деньги поступили на счет. Все четко! Молодцы!
					</div>

					<CommentBox
						comments={comments}
						onAddComment={onAddComment}
						onDeleteComment={onDeleteComment} />
				</section>

			</div>
		)
	}
}

MainSection.propTypes = {
	comments: PropTypes.array.isRequired
};

export default MainSection